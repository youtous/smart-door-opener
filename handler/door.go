package handler

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"regexp"
	"strings"
)
import "github.com/go-resty/resty/v2"

type Door struct {
	secretCode     string
	doorWebHookURL string
	httpClient     *resty.Client
}

func (h *Door) GetOpenDoor(c echo.Context) error {
	if c.Param("accessCode") != h.secretCode {
		return c.Render(http.StatusForbidden, "wrongCode", echo.Map{
			"title": "Bienvenue à Saint-Aubin ! Arrivée autonome",
		})
	}

	doorCode := c.Param("doorCode")
	if !isValidDoorCode(doorCode) {
		return c.Render(http.StatusForbidden, "wrongCode", echo.Map{
			"title": "Bienvenue à Saint-Aubin ! Arrivée autonome",
		})
	}
	formattedDoorCode := h.formatDoorCode(doorCode)

	return c.Render(http.StatusOK, i8nGetIndexTemplate(c), echo.Map{
		"title":             "Bienvenue à Saint-Aubin ! Arrivée autonome",
		"formattedDoorCode": formattedDoorCode,
		"csrfToken":         c.Get(middleware.DefaultCSRFConfig.ContextKey).(string),
	})
}

func (h *Door) PostOpenDoor(c echo.Context) error {
	if c.Param("accessCode") != h.secretCode {
		return c.Render(http.StatusForbidden, "wrongCode", echo.Map{
			"title": "Bienvenue à Saint-Aubin ! Arrivée autonome - Mauvais code",
		})
	}

	// open the door
	resp, respErr := h.httpClient.R().Get(h.doorWebHookURL)

	successMessage := ""
	errorMessage := ""
	if respErr != nil {
		c.Logger().Error("Received an error while triggering webhook door event: %v", respErr)
		errorMessage = "Une erreur est survenue lors de l'ouverture, veuillez nous contacter."
	} else {
		c.Logger().Info(fmt.Sprintf("Door opening trigger accepted: %v", resp))
		successMessage = "La commande d'ouverture de porte a été exécutée, elle devrait s'effectuer dans quelques secondes, poussez la porte."
	}

	doorCode := c.Param("doorCode")
	if !isValidDoorCode(doorCode) {
		// Handle invalid input
		return c.Render(http.StatusForbidden, "wrongCode", echo.Map{
			"title": "Bienvenue à Saint-Aubin ! Arrivée autonome",
		})
	}
	formattedDoorCode := h.formatDoorCode(doorCode)

	return c.Render(http.StatusOK, i8nGetIndexTemplate(c), echo.Map{
		"title":             "Bienvenue à Saint-Aubin ! Arrivée autonome - Ouverture de porte",
		"formattedDoorCode": formattedDoorCode,
		"csrfToken":         c.Get(middleware.DefaultCSRFConfig.ContextKey).(string),
		"errorMessage":      errorMessage,
		"successMessage":    successMessage,
	})
}

func i8nGetIndexTemplate(c echo.Context) string {
	lang := c.Param("lang")
	templateName := "index"
	if lang == "en" {
		templateName = "index_en"
	}
	return templateName
}

func (h *Door) formatDoorCode(doorCode string) string {
	// Format the door code with spaces between numbers
	formattedDoorCode := strings.Join(strings.Split(doorCode, ""), " ")
	return formattedDoorCode
}

// isValidDoorCode checks if the doorCode consists only of digits
func isValidDoorCode(doorCode string) bool {
	regex := regexp.MustCompile(`^\d+$`)
	return regex.MatchString(doorCode)
}

func NewHandlerDoor(secretCode string, doorWebHookURL string) *Door {
	doorHandler := &Door{
		secretCode:     secretCode,
		doorWebHookURL: doorWebHookURL,
		httpClient:     resty.New(),
	}
	doorHandler.httpClient.SetHeader("Content-Type", "application/json")
	doorHandler.httpClient.SetHeader("Accept", "application/json")

	return doorHandler
}
