# Smart Door Opener

[![pipeline status](https://gitlab.com/youtous/smart-door-opener/badges/main/pipeline.svg)](https://gitlab.com/youtous/smart-door-opener/-/commits/main)
[![Docker image](https://img.shields.io/badge/image-registry.gitlab.com%2Fyoutous%2Fsmart--door--opener-e4f0fb?logo=docker)](https://gitlab.com/youtous/smart-door-opener/container_registry)
[![GitHub Repo stars](https://img.shields.io/github/stars/youtous/smart-door-opener?label=✨%20youtous%2Fsmart-door-opener&style=social)](https://github.com/youtous/smart-door-opener/)
[![Gitlab Repo](https://img.shields.io/badge/gitlab.com%2Fyoutous%2Fsmart--door--opener?label=✨%20youtous%2Fsmart-door-opener&style=social&logo=gitlab)](https://gitlab.com/youtous/smart-door-opener/)
[![Licence](https://img.shields.io/github/license/youtous/smart-door-opener)](https://github.com/youtous/smart-door-opener/blob/main/LICENSE)


This application open a door remotely using Sequematic and a FingerBot.
This cheap materials (~40$) enables remote opening of buildings for Airbnb rentals.

## How to?

* Buy a FingerBot and its associated gateway on aliexpress or other : https://www.aliexpress.com/w/wholesale-fingerbot.html.
* Configure Smart Home and link it with a scene on Sequematic : https://sequematic.com
* The webhook should be configured to trigger the SmartHome Scene.

## Usage

```text
╰─λ ./smart-door-opener server --help                                                                                                                                                                                    0 < 16:50:15
Usage: server server --web-hook-door-url=STRING --access-secret-code=STRING

Start the server.

Flags:
  -h, --help                         Show context-sensitive help.

      --web-hook-door-url=STRING     HTTP URL of the webhook to trigger door opening ($SERVER_WEB_HOOK_DOOR_URL).
      --access-secret-code=STRING    Secret code used to access the opening page ($SERVER_ACCESS_SECRET_CODE).
      --log-level="info"             Log level of the App ($LOG_LEVEL).
```

Host this application on your favorite web hosting server and enjoy!
Feel free to fork for translations.

## License

MIT