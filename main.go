package main

import (
	"context"
	"fmt"
	"github.com/alecthomas/kong"
	"github.com/brpaz/echozap"
	"github.com/foolin/goview/supports/echoview-v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"go.uber.org/zap"
	"net/http"
	"os"
	"os/signal"
	"smart-door-opener/handler"
	"strings"
	"time"
)

var CLI struct {
	Server struct {
		WebHookDoorURL   string `help:"HTTP URL of the webhook to trigger door opening." env:"SERVER_WEB_HOOK_DOOR_URL" required:""`
		AccessSecretCode string `help:"Secret code used to access the opening page." env:"SERVER_ACCESS_SECRET_CODE" required:""`
		LogLevel         string `help:"Log level of the App." env:"LOG_LEVEL" default:"info"`
	} `cmd:"" help:"Start the server."`
}

func main() {
	// logger
	zapLogger, _ := zap.NewProduction()

	// command loading
	ctxCmd := kong.Parse(&CLI,
		kong.Name("server"),
		kong.Description("A web server app."),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
			Summary: true,
		}))
	switch ctxCmd.Command() {
	case "server":
	default:
		zapLogger.Fatal(fmt.Sprintf("Unkown command: %v", ctxCmd.Command()))
	}

	e := echo.New()

	level := zapLogger.Level()
	errLogLevel := level.Set(CLI.Server.LogLevel)
	if errLogLevel != nil {
		e.Logger.Fatal(errLogLevel.Error())
	}
	logLevel := ConvertLogLevel(CLI.Server.LogLevel)
	e.Logger.SetLevel(logLevel)

	// load middlewares
	e.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
		TokenLookup: "form:csrf",
	}))
	e.Use(echozap.ZapLogger(zapLogger))
	e.Use(middleware.Recover())
	e.Use(middleware.RequestID())
	e.Static("/static", "static")

	// Set Renderer
	e.Renderer = echoview.Default()

	// Handlers
	doorHandler := handler.NewHandlerDoor(
		CLI.Server.AccessSecretCode,
		CLI.Server.WebHookDoorURL,
	)

	e.GET("/", doorHandler.GetOpenDoor)
	e.GET("/:lang/:accessCode/:doorCode", doorHandler.GetOpenDoor)
	e.POST("/:lang/:accessCode/:doorCode", doorHandler.PostOpenDoor)
	e.GET("/_health", func(c echo.Context) error {
		return c.String(http.StatusOK, "RUNNING")
	})

	// Start server
	go func() {
		if err := e.Start(":1323"); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

func ConvertLogLevel(level string) log.Lvl {
	switch strings.ToUpper(level) {
	case "DEBUG":
		return log.DEBUG
	case "INFO":
		return log.INFO
	case "WARN":
		return log.WARN
	case "ERROR":
		return log.ERROR
	case "OFF":
		return log.OFF
	default:
		return log.INFO
	}
}
